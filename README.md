# Weather App

An application to show the weather of a selected location.
The locations are: London, New York, Mumbai, Sydney and Tokyo.

The first page shows a the basic current weather, where the user can select forecast to be redirected to the second page, a 16 day forecast.

This is a single page application built with React and styled components. 
Using cross-fetch to fetch the data.


## Development

If I was to further develop this application further my next tasks would be to introduce a dotenv file and move the url and key into it, leaving the extensions on the fetch requests.

To improve the speed I would intoduce lazyloading or create a image component which would have a placeholder svg and render the image onload.

The list of locations are held in a helper, this is to make it easier to manage.

To make the site more personalised you could introcude a form of state managment (Redux) and save the users desired primary location.


## Installation and Setup


You will need node.js and npm installed globally on your machine.

### steps

	- Clone this repository
	
	- Run "npm install" to install the packages
	
	- Run "npm start"
	
	- Visit localhost:3000
	


