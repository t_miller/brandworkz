export const locations: Array<{ city: string; country: string }> = [
  { city: "London", country: "UK" },
  { city: "New York", country: "US" },
  { city: "Mumbai", country: "IN" },
  { city: "Sydney", country: "AU" },
  { city: "Tokyo", country: "JP" },
];
