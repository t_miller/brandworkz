import styled, { CSSObject } from "styled-components";

export const StyledItemCon = styled.div<{
  theme: { breakpoints: CSSObject };
}>`
  @media (min-width: ${props => props.theme.breakPoints.mobile}) {
    display: flex;
    overflow-x: auto;
    overflow-y: hidden;
    scroll-snap-type: x mandatory;
    align-items: center;
    width: auto;

    -ms-overflow-style: none;
    scrollbar-width: none;
    &::-webkit-scrollbar {
      display: none;
    }
  }
`;

export const StyledInner = styled.div<{
  theme: { breakpoints: CSSObject };
}>`
  display: flex;
  margin-top: 5rem;

  & > div {
    width: 50%;
  }

  @media (max-width: ${props => props.theme.breakPoints.tablet}) {
    margin-top: 2.5rem;
  }

  @media (max-width: ${props => props.theme.breakPoints.mobile}) {
    flex-direction: column;
    margin-top: 0;

    & > div {
      width: 100%;
    }
  }
`;
