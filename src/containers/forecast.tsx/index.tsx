import React, { FunctionComponent, useState, useEffect } from "react";
import { locations } from "../../helpers/locations";
import { fetchForecast } from "./fetchForecast";
import {
  WeatherDetails,
  ForecastItem,
  Hero,
  Container,
  TempSliders
} from "../../components";
import { StyledItemCon, StyledInner } from "./partials";

type ForecastStateTypes = {
  temp: number;
  app_temp: number;
  weather: { icon: string; code: number; description: string };
  high_temp: number;
  low_temp: number;
  datetime: string;
};

const weatherObj = {
  app_temp: 0,
  sunrise: "",
  sunset: "",
  temp: 0,
  weather: { icon: "", code: 0, description: "" },
  high_temp: 0,
  low_temp: 0,
  datetime: ""
};

const Forecast: FunctionComponent = () => {
  const [weather, SetWeather] = useState<{
    current: ForecastStateTypes;
    forecast: ForecastStateTypes[];
  }>({
    current: weatherObj,
    forecast: []
  });
  const [minTemp, SetMinTemp] = useState(-10);
  const [maxTemp, SetMaxTemp] = useState(50);
  const params = new URLSearchParams(window.location.search);
  const city = params.get("city") || "";

  useEffect(() => {
    const setWeather = async (city: string) => {
      const loc: { city: string; country: string } = locations.filter(
        e => e.city === city
      )[0];
      const currentForecast = await fetchForecast(city, loc.country);

      if (currentForecast && currentForecast.data?.length > 0) {
        const getCurrentWeather = currentForecast.data.shift();

        SetWeather({
          current: getCurrentWeather,
          forecast: currentForecast.data
        });
      }
    };
    setWeather(city);
  }, [locations]);

  const minTempHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const temp = parseInt(value);
    if (temp <= maxTemp) {
      SetMinTemp(temp);
    }
  };

  const maxTempHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const temp = parseInt(value);
    if (temp >= minTemp) {
      SetMaxTemp(temp);
    }
  };

  const tempHandler = (min: number, max: number) => {
    return minTemp < min && maxTemp > max;
  };

  return (
    <div>
      <Container>
        <Hero title="Forecast (16 day)" />
        <StyledInner>
          {weather.current && (
            <WeatherDetails
              city={city}
              temp={weather.current.temp}
              feelsLikeTemp={weather.current.app_temp}
              icon={weather.current.weather?.icon || ""}
              description={weather.current.weather?.description || ""}
              includeHref={false}
            />
          )}
          <TempSliders
            minTemp={minTemp}
            maxTemp={maxTemp}
            minTempHandler={minTempHandler}
            maxTempHandler={maxTempHandler}
          />
        </StyledInner>
        {weather.forecast.length > 0 && (
          <StyledItemCon>
            {weather.forecast.map(
              (d, i) =>
                tempHandler(d.low_temp, d.high_temp) && (
                  <ForecastItem
                    key={i}
                    temp={d.temp}
                    highTemp={d.high_temp}
                    lowTemp={d.low_temp}
                    icon={d.weather.icon}
                    description={d.weather.description}
                    dateTime={d.datetime}
                  />
                )
            )}
          </StyledItemCon>
        )}
      </Container>
    </div>
  );
};

export default Forecast;
