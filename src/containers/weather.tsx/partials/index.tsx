import styled, { CSSObject } from "styled-components";

export const StyledInnter = styled.div<{
  theme: { breakpoints: CSSObject };
}>`
  display: flex;
  margin-top: 2rem;

  @media (max-width: ${props => props.theme.breakPoints.tablet}) {
    flex-direction: column;
  }
`;
