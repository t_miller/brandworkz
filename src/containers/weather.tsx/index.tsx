import React, { FunctionComponent, useState } from "react";
import { locations } from "../../helpers/locations";
import {
  Theme,
  Container,
  CurrentWeather,
  CitiesFilter,
  Hero
} from "../../components";
import { StyledInnter } from "./partials";

const Weather: FunctionComponent = () => {
  const [city, SetCity] = useState("London");

  const onCityHandler = (userCity: string) => {
    console.log("city", userCity);
    SetCity(userCity);
  };

  return (
    <Theme>
      <Container>
        <Hero title="Current weather" />
        <StyledInnter>
          <CitiesFilter
            citiesArr={locations}
            onCityHandler={onCityHandler}
            userCity={city}
          />
          <CurrentWeather city={city} locations={locations} />
        </StyledInnter>
      </Container>
    </Theme>
  );
};

export default Weather;
