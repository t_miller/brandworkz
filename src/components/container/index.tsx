import React, { FunctionComponent } from "react";
import { StyledContainer } from "./partials";

const Container: FunctionComponent = ({ children }) => {
  return <StyledContainer>{children}</StyledContainer>;
};

export default Container;
