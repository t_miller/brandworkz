import styled, { CSSObject } from "styled-components";

export const StyledButton = styled.button<{
  theme: { colors: CSSObject; breakpoints: CSSObject; typography: CSSObject };
  isSelected: boolean;
}>`
  width: 100%;
  display: flex;
  justify-content: center;
  padding: 1rem;
  background: ${props => props.theme.colors.white};
  box-shadow: 0px 3px 6px ${props => props.theme.colors.boxShadow};
  border: none;
  cursor: pointer;
  ${props => props.theme.typography.bodyLarge}

  @media (min-width: calc(${props => props.theme.breakPoints.tablet} + 1px)) {
      &:hover {
        background: ${props => props.theme.colors.dark20};
      }
  }

  ${props =>
    props.isSelected
      ? `color: ${props.theme.colors.white}; background: ${props.theme.colors.primary}`
      : ""}
`;

export const StyledIcon = styled.div`
  margin-left: 1rem;
`;
