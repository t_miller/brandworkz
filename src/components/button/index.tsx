import React, { FunctionComponent } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { StyledButton, StyledIcon } from "./partials";

type ButtonProps = {
  label: string;
  onClickHandler?: () => void;
  isSelected?: boolean;
  icon?: IconProp;
};

const Button: FunctionComponent<ButtonProps> = ({
  label = "",
  onClickHandler,
  isSelected = false,
  icon
}) => (
  <StyledButton isSelected={isSelected} onClick={onClickHandler}>
    <span>{label}</span>
    {icon && (
      <StyledIcon>
        <FontAwesomeIcon icon={icon} />
      </StyledIcon>
    )}
  </StyledButton>
);

export default Button;
