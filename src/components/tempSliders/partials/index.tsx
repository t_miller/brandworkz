import styled, { CSSObject } from "styled-components";

export const StyledContainer = styled.div<{
  theme: { colors: CSSObject; breakPoints: CSSObject };
}>`
  display: flex;
  justify-content: center;
  padding: 1rem;

  @media (max-width: ${props => props.theme.breakPoints.tablet}) {
    flex-direction: column;
  }
`;

export const StyledInner = styled.div<{
  theme: { typography: CSSObject; breakPoints: CSSObject };
}>`
  width: 50%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  padding: 1rem;
  ${props => props.theme.typography.bodyLarge}

  @media (max-width: ${props => props.theme.breakPoints.tablet}) {
   width: 100%;
  }
`;

export const StyledInput = styled.input`
  width: 100%;
  -webkit-appearance: none;
  cursor: pointer;
`;
