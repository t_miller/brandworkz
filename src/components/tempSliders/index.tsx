import React, { FunctionComponent } from "react";
import { StyledContainer, StyledInner, StyledInput } from "./partials";

type TempSlidersProps = {
  minTemp: number;
  maxTemp: number;
  minTempHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
  maxTempHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

const TempSliders: FunctionComponent<TempSlidersProps> = ({
  minTemp = -10,
  maxTemp = 50,
  minTempHandler,
  maxTempHandler
}) => (
  <StyledContainer>
    <StyledInner>
      <div>
        <span>Min temperature </span>
        <span>{minTemp} °C</span>
      </div>
      <StyledInput
        type="range"
        value={minTemp}
        min={-10}
        max={50}
        step={1}
        onChange={minTempHandler}
      />
    </StyledInner>
    <StyledInner>
      <div>
        <span>Max temperature </span>
        <span>{maxTemp} °C</span>
      </div>
      <StyledInput
        type="range"
        value={maxTemp}
        min={-10}
        max={50}
        step={1}
        onChange={maxTempHandler}
      />
    </StyledInner>
  </StyledContainer>
);

export default TempSliders;
