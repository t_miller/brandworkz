import fetch from "cross-fetch";

export const fetchWeather = (city: string, country: string) => {
  const key = "59b18db7c8754778bb98e0688c3bb6fa";
  return fetch(
    `https://api.weatherbit.io/v2.0/current?city=${city}&country=${country}&key=${key}`
  )
    .then(res => res.json())
    .then(res => {
      return res;
    })
    .catch(error => console.error("Fetch error ", error));
};
