import React, { FunctionComponent, useEffect, useState } from "react";
import { fetchWeather } from "./fetchWeather";
import { WeatherDetails } from "../";

type CurrentWeatherProps = {
  city: string;
  locations: { city: string; country: string }[];
};

const CurrentWeather: FunctionComponent<CurrentWeatherProps> = ({
  city = "",
  locations = []
}) => {
  const [cityWeather, SetCityWeather] = useState<{
    temp: number;
    app_temp: number;
    weather: { icon: string; code: number; description: string };
    sunrise: string;
    sunset: string;
  }>({
    app_temp: 0,
    sunrise: "",
    sunset: "",
    temp: 0,
    weather: { icon: "", code: 0, description: "" }
  });

  useEffect(() => {
    const setWeather = async (city: string) => {
      const loc: { city: string; country: string } = locations.filter(
        e => e.city === city
      )[0];
      const weather = await fetchWeather(city, loc.country);

      if (weather && weather.data?.length > 0) {
        SetCityWeather(weather.data[0]);
      }
    };
    setWeather(city);
  }, [city, locations]);

  return (
    <>
      {cityWeather && (
        <WeatherDetails
          city={city}
          temp={cityWeather.temp}
          feelsLikeTemp={cityWeather.app_temp}
          icon={cityWeather.weather.icon}
          description={cityWeather.weather.description}
          sunrise={cityWeather.sunrise}
          sunset={cityWeather.sunset}
        />
      )}
    </>
  );
};

export default CurrentWeather;
