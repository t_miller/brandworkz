import styled, { CSSObject } from "styled-components";

export const StyledHeader = styled.header<{
  theme: { colors: CSSObject };
}>`
  height: 8vh;
  width: 100%;
  background: ${props => props.theme.colors.white};
`;
