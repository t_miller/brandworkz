import styled, { CSSObject } from "styled-components";

export const StyledHero = styled.div<{
  theme: { typography: CSSObject };
}>`
  ${props => props.theme.typography.heading};
`;
