import React, { FunctionComponent } from "react";

import { StyledHero } from "./partials";

type HeroProps = {
  title: string;
};

const Hero: FunctionComponent<HeroProps> = ({ title = "" }) => (
  <StyledHero>
    <h1>{title}</h1>
  </StyledHero>
);

export default Hero;
