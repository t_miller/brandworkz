import styled, { CSSObject } from "styled-components";

export const StyledContainer = styled.div<{
  theme: { colors: CSSObject; breakpoints: CSSObject };
}>`
  width: 100%;
  padding: 1rem;
  background: ${props => props.theme.colors.white};
  box-shadow: 0px 3px 6px ${props => props.theme.colors.boxShadow};
  width: 50%;

  @media (max-width: ${props => props.theme.breakPoints.tablet}) {
    width: 100%;
  }
`;

export const StyledInner = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
`;

export const StyledHref = styled.h2<{
  theme: { typography: CSSObject };
}>`
  & a {
    text-decoration: none;
  }

  @media (max-width: ${props => props.theme.breakPoints.mobile}) {
    margin-top: 2rem;
  }
`;

export const StyledInnerContainer = styled.div<{
  theme: { breakPoints: CSSObject };
}>`
  display: flex;
  justify-content: space-between;

  & > div,
  ${StyledHref} {
    width: 50%;
  }

  @media (max-width: ${props => props.theme.breakPoints.mobile}) {
    flex-direction: column;

    & > div,
    ${StyledHref} {
      width: 100%;
    }
  }
`;

export const StyledHeading = styled.h2<{
  theme: { typography: CSSObject };
}>`
  ${props => props.theme.typography.heading};

  &:nth-of-type(2) {
    margin-bottom: 0;
  }
`;

export const StyledBody = styled.div<{
  theme: { typography: CSSObject };
}>`
  ${props => props.theme.typography.body};
`;

export const StyledImg = styled.div<{
  isLoaded: boolean;
}>`
  display: ${props => (props.isLoaded ? "initial" : "none")};
`;
