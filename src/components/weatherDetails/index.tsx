import React, { FunctionComponent, useState } from "react";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { Button } from "../";
import {
  StyledContainer,
  StyledInner,
  StyledHeading,
  StyledBody,
  StyledImg,
  StyledInnerContainer,
  StyledHref
} from "./partials";

type WeatherDetailsProps = {
  city: string;
  temp: number;
  feelsLikeTemp?: number;
  icon: string;
  description: string;
  sunrise?: string;
  sunset?: string;
  includeHref?: boolean;
};

const WeatherDetails: FunctionComponent<WeatherDetailsProps> = ({
  city,
  temp,
  feelsLikeTemp,
  icon,
  description,
  sunrise,
  sunset,
  includeHref = true
}) => {
  const [iconLoaded, SetIconLoaded] = useState<boolean>(false);

  const iconLoadHandler = () => {
    SetIconLoaded(true);
  };

  return (
    <StyledContainer>
      <StyledInner>
        <div>
          <StyledHeading>{city}</StyledHeading>
          <StyledHeading>
            <span>{temp} °C</span>
          </StyledHeading>
          {description && (
            <StyledBody>
              <span>{description}</span>
            </StyledBody>
          )}
        </div>
        {icon && (
          <StyledImg isLoaded={iconLoaded}>
            <img
              src={`https://www.weatherbit.io/static/img/icons/${icon}.png`}
              alt="icon"
              onLoad={iconLoadHandler}
            />
          </StyledImg>
        )}
      </StyledInner>
      <StyledInnerContainer>
        <div>
          {feelsLikeTemp && (
            <StyledBody>
              <span>Feels like: {feelsLikeTemp} °C</span>
            </StyledBody>
          )}
          {sunrise && (
            <StyledBody>
              <span>Sunrise: {sunrise}</span>
            </StyledBody>
          )}
          {sunset && (
            <StyledBody>
              <span>Sunrise: {sunset}</span>
            </StyledBody>
          )}
        </div>
        {includeHref && (
          <StyledHref>
            <a href={`/forecast?city=${encodeURI(city)}`}>
              <Button label="Forecast" icon={faArrowRight} />
            </a>
          </StyledHref>
        )}
      </StyledInnerContainer>
    </StyledContainer>
  );
};

export default WeatherDetails;
