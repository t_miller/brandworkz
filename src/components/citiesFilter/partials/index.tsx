import styled, { CSSObject } from "styled-components";

export const StyledCitiesFilter = styled.div<{
  theme: { breakpoints: CSSObject };
}>`
  width: 50%;
  display: flex;
  flex-wrap: wrap;
  margin-right: 1rem;

  @media (max-width: ${props => props.theme.breakPoints.tablet}) {
    width: 100%;
  }
`;

export const StyledButton = styled.div<{
  theme: { colors: CSSObject; breakpoints: CSSObject };
}>`
  width: calc(50% - 1rem);
  margin-right: 1rem;

  &:nth-child(odd) {
    margin: 1rem 1rem 1rem 0;
  }

  &:nth-child(even) {
    margin: 1rem 0 1rem 1rem;
  }

  &:nth-child(1) {
    margin: 0 1rem 1rem 0;
  }

  &:nth-child(2) {
    margin: 0 0 1rem 1rem;
  }

  @media (max-width: ${props => props.theme.breakPoints.mobile}) {
    width: 100%;

    &:nth-child(1n) {
      margin: 0.5rem 0;
    }
  }
`;
