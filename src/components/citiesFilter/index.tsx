import React, { FunctionComponent } from "react";
import { Button } from "../../components";
import { StyledCitiesFilter, StyledButton } from "./partials";

type CitiesFilterProps = {
  citiesArr: { city: string; country: string }[];
  onCityHandler: (userCity: string) => void;
  userCity: string;
};

const CitiesFilter: FunctionComponent<CitiesFilterProps> = ({
  citiesArr = [],
  onCityHandler,
  userCity
}) => {
  return (
    <StyledCitiesFilter>
      {citiesArr.length > 0 &&
        citiesArr.map((e, i) => (
          <StyledButton key={i}>
            <Button
              label={e.city}
              onClickHandler={() => onCityHandler(e.city)}
              isSelected={userCity === e.city}
            />
          </StyledButton>
        ))}
    </StyledCitiesFilter>
  );
};

export default CitiesFilter;
