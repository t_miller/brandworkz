import styled, { CSSObject } from "styled-components";

export const StyledContainer = styled.div<{
  theme: { breakpoints: CSSObject; theme: CSSObject; colors: CSSObject };
}>`
  width: 10rem;
  padding: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  ${props => `border: 2px solid ${props.theme.colors.dark20}; 
  background: ${props.theme.colors.white};
  `};

  @media (min-width: ${props => props.theme.breakPoints.mobile}) {
    &:not(:last-child) {
      border-right: none;
    }
  }

  @media (max-width: ${props => props.theme.breakPoints.mobile}) {
    width: 100%;
    flex-direction: row;
    justify-content: space-between;

    &:not(:last-child) {
      border-bottom: none;
    }
  }
`;

export const StyledInner = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1rem;
`;

export const StyledTemp = styled.div<{
  theme: { typography: CSSObject };
}>`
  ${props => props.theme.typography.bodyLarge};
  text-align: center;
`;

export const StyledBody = styled.div<{
  theme: { typography: CSSObject; breakPoints: CSSObject };
}>`
  ${props => `${props.theme.typography.body}`};
  text-align: center;

  @media (max-width: ${props => props.theme.breakPoints.mobile}) {
    margin: 1rem 0;
  }
`;

export const StyledDescription = styled.div<{
  theme: { typography: CSSObject; breakPoints: CSSObject };
}>`
  ${props => `${props.theme.typography.body}`};
  min-height: 50px;
  text-align: center;

  @media (max-width: ${props => props.theme.breakPoints.mobile}) {
    margin: 1rem 0;
  }
`;

export const StyledDetail = styled.div<{
  theme: { typography: CSSObject };
}>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  ${props => props.theme.typography.body};
`;

export const StyledImg = styled.div<{
  isLoaded: boolean;
}>`
  display: ${props => (props.isLoaded ? "initial" : "none")};
`;
