import React, { FunctionComponent, useState } from "react";
import {
  StyledContainer,
  StyledTemp,
  StyledBody,
  StyledDetail,
  StyledImg,
  StyledDescription
} from "./partials";

type ForecastItemProps = {
  temp: number;
  highTemp: number;
  lowTemp: number;
  icon: string;
  description?: string;
  dateTime?: string;
};

const ForecastItem: FunctionComponent<ForecastItemProps> = ({
  temp,
  highTemp,
  lowTemp,
  icon,
  description,
  dateTime
}) => {
  const [iconLoaded, SetIconLoaded] = useState<boolean>(false);

  const iconLoadHandler = () => {
    SetIconLoaded(true);
  };

  const formateDate = (date: string) => {
    return date
      .toString()
      .split("-")
      .reverse()
      .join("-");
  };

  return (
    <StyledContainer>
      <div>
        {dateTime && (
          <StyledBody>
            <span>{formateDate(dateTime)}</span>
          </StyledBody>
        )}
        <StyledTemp>
          <span>{temp} °C</span>
        </StyledTemp>
        {description && (
          <StyledDescription>
            <span>{description}</span>
          </StyledDescription>
        )}
      </div>
      <div>
        {icon && (
          <StyledImg isLoaded={iconLoaded}>
            <img
              src={`https://www.weatherbit.io/static/img/icons/${icon}.png`}
              alt="icon"
              onLoad={iconLoadHandler}
            />
          </StyledImg>
        )}
        <StyledDetail>
          <span>high - low</span>
          <span>
            {highTemp} °C - {lowTemp} °C
          </span>
        </StyledDetail>
      </div>
    </StyledContainer>
  );
};

export default ForecastItem;
