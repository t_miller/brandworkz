import { FunctionComponent } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Theme, Header } from "./components";
import { routes } from "./utils/routes";

const App: FunctionComponent = () => {
  return (
    <Router>
      <Theme>
        <Header />
        {routes.map((route, i) => (
          <Route key={i} {...route} />
        ))}
      </Theme>
    </Router>
  );
};

export default App;
