import { FunctionComponent } from "react";
import Weather from "../containers/weather.tsx";
import Forecast from "../containers/forecast.tsx";

export const routes: Array<{
  name: string;
  exact: boolean;
  path: string;
  component: FunctionComponent<{}>;
}> = [
  {
    name: "Current weather",
    exact: true,
    path: "/",
    component: Weather
  },
  {
    name: "Forecast (16 day)",
    exact: true,
    path: "/forecast",
    component: Forecast
  }
];
